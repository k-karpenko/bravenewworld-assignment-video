# Videq 1.0

### Overview

This project is a pure client-side application with no backend required for it to be
able to functioning properly.

It is based on **React.js** and uses a set of HTML5 APIs to implement some **RIA** features 
and most importantly to display video content.

Down below you can find detailed overview of the project build infrastructure and
explanations for a different architectural decisions made during its development.

### Getting started

Project uses few external tools in order to manage dependencies list and automate
its build process.

Before going further, you will need to have these tools installed on your local machine.

#### MacOS X

Easiest way is to use `brew`. So, if you don't have it installed yet, please do that.
Here is its official site where you can also find detailed instructions on what to 
do: http://brew.sh/index.html

When it is done, just run this command:
> `brew install npm`

#### Windows

As there is no feasible dependencies management tool available for Windows users,
you will need to install Node.js by yourself.

Here is a link: http://nodejs.org/

#### Project setup

As you've already cloned project from the Git repository, you just need to feed it to
NPM to fetch all side dependencies it declares. It is easy, just run:

> `npm install`

And we will also need some sort of static HTTP server to server project files to user. This 
is the tool I use:

> `npm install -g http-server`

If everything went fine, at this stage you should have a project which is ready to be 
compiled. I hope you are the lucky one and it is true, so you can just bake it with 
Webpack:

> `webpack`

Or if you want to have production build (optimized + minified):

> `webpack -p`

Build output is available in `www` at this point. It is static video file, bundled as a part
of application and actual build product `app.js`. It has both JS and CSS content of 
application by itself and also its dependencies bundled in it.

To access live application, just start web-server (`http-server` or whatever) in `www`
directory:
> cd www 

> http-server -p 8083

And open `http://localhost:8083`

### Overview

Project has been build on ES6 + React.js and Babel + Webpack technologies stack.

React.js was chosen mainly because of its rich mechanisms for application lifecycle
control and high level of abstraction from actual DOM-tree manipulations. It is important
because of two main reasons:
- It improves application overall performance by reducing unnecessary redraws (if application 
developed properly)
- Resulted code has high level of separation between code for logic and its visual representation

React also highly improves granularity of application, especially when used with Redux.

> **Note**: Across all application components, I'm using `react-addons-update` in order to keep component state
as a new object upon every update. 

In this case I decided not to use Redux, as application components has almost no state associated with
them. So Redux, if used, will only slow down development process.

Finally, all project components were created with JSX fixtures which highly improves code 
readability and compiler/IDE ability to formally validate its input arguments 
correctness.

As regarding to ES6, it is used only as a sourcecode language in order to get access to newest
language constructs. Eventually, it will be transpiled into ES5 by `babel`.

At this point, I need to give you a slight overview into the project build infrastructure.

It is quite common, and implemented with `webpack` and `babel`. Webpack is responsible for
project resources location and their pre/post processing. Actual processing done by loaders (plugged-in) integrated 
into a some phase of the build workflow. 

In a case of this project there are few plugins I'm using:
- **babel-loader**: Main loader responsible for JSX compilation and ES6 transpiling 
- **autoprefixer**: 

Takes source `*.css` file and locate all rules that can be different in versions of popular 
browser platforms. If there are any such rules located, it will generate all possible variants
for them so they will be supported on old platforms.

Good example of this technique is a case of `display: flex`. It is not supported on Android 4.3 out of box,
but autoprefixer solves this problem by providing webkit specific attribute:

```
display: -webkit-box;
display: -ms-flexbox;
display: flex
```

- **css-loader**: Loads CSS into the resulting `*.js` file

### Project specifics

Due to the initial requirements, user should have ability to watch and interact with a
video file represented as a normal 2D video and also in the same video transformed into
3D shape.

Transition between 2D and 3D mode done by touching 2D video player which triggers transition
to the 3D display mode.

Also, one of requirements says that there are some percent of users that do not have access
to all Web API required to display content in they browsers. So, video should be displayed in 
some way that do not requires user's browser to support Video API.

To implement these requirements, I have split application into **three** main components
integrated into the single entry point called **Viewport**:

- Viewer2D
- Viewer3D
- ViewerStatic

#### Viewer2D

This is a simplest viewer that provides user with a video preview in a fullscreen mode.

To make video component responsive and displaying properly on screens with different
dimensions, I've used CSS properties `object-fit` and `object-position`. First attribute,
when set to `cover`, tells browser that related component should have height and width
same as those of its parent container. But when scaled, component preserve its initial
aspect ratio.

Only one problem exists with this approach - lack of support on IE (11/Edge) platform. In 
order to bypass this limitation we can use polyfill for IE and other platform with no suppport
for this attribute.

I've decided to use `object-fit-polyfill` (https://github.com/constancecchen/object-fit-polyfill) 
as a polyfill solution. It has three main features I was looking for: **a)** compatibility with old platforms;
**b)** support for `<video/>` tag; **c)**    it is active project with commits made during the last two months

To satisfy requirements for minimal height and width of component when it no longer 
scales below, I added `min-width` and `min-height` attributes. 

`video` component is responsible for actual media content display. It gets all necessary
information from `props` provided by Viewport component. 

**Note**: At the moment, application supports only static video file encoded as MPEG4 stream. In order
to support wider users base, it is necessary to provide different versions of video or at least
**WebM** and **Ogg**.

When user touches screen with video, he'll be transitioned to the 3D viewer. It works this way:
1. **Viewport** passes function reference into the **Viewport2D** props
2. As a reaction on the touch event, **Viewport2D** invokes function passed by **Viewport**
3. Viewport switches viewer context by updating its own state (mode: 2d -> 3d)

Transition between components will eventually unmount components of **Viewer2D** from the 
DOM tree.

#### Viewer2D mobile tilt/shift



#### Viewer3D

This viewer is quite more difficult comparing to **Viewer2D** and requires user to have 
WebGL support enabled in the browser.

Due to business requirements, we need to create 3D viewer where given video file we'll be
mapped on each side of cube with a different time offset on each. Cube should be rotated
each time user move scene with mouse/touch.

So, we have few problems to think about:
- How and when should be initialize GL context in our React component?
- Each side of cube should have video with a different time offset, but we have just a 
single video stream.
- User should be able to rotate cube both with mouse and touch events

Okay. First of all we need to sort out 3D pipeline. There is only one feasible option for us
to draw 3D on most of all browsers - Canvas and ``webgl``-context. Other solutions like Unity or 
Flash Player, are either outdated or require additional software to be installed.

Now we need to initialize context. To do that we need to have reference on rendering `canvas`, 
which we will get as a reference during its mounting. So, it is a good idea to bind init logic
on `componentDidMount` handler so we can be sure about `canvas` presence in references list.

> **Note**: React calls `componentDidMount` only once when all components created inside `render()` method
were successfully mounted into the DOM-tree. All successive updated into the component state will trigger
`componentDidUpdate` method instead.

> Also, please note how we're obtaining reference on `canvas` in the code presented down below:
> ```
> <canvas  ref={c => this._refs.canvas3d = c} .../>
> ```
> This style (callback-based) is recommended by Facebook, as it makes references obtain and persist mechanisms
> more explicit and type-safe (in a case of TypeScript).

Let's have a look on the actual `componentDidMount` implementation:
```js
1 | componentDidMount() {
2 |    const videos = this._refs.videos;
3 |    const canvas3d = this._refs.canvas3d;
4 |
5 |    canvas3d.width = canvas3d.clientWidth;
6 |    canvas3d.height = canvas3d.clientHeight;
7 |
8 |    this.perspectiveMatrix = makePerspective(30, canvas3d.width / canvas3d.height, 1, -5);
9 |
10|    const metaFetchInterval = setInterval(() => {
11|        if (videos.filter(v => v !== null && v.readyState <= 0).length === 0) {
12|            this.show3DScreen(videos, canvas3d);
13|            clearInterval(metaFetchInterval);
14|        }
15|    }, 50);
16| }
```

Here we are setting canvas drawing area dimensions equal to the actual canvas 
DOM-element size calculated based on CSS rules applied to him (100% for both height and width).
This size will be actual WebGL viewport size.

Then, on the line 8th we are defining `perspectiveMatrix`. This matrix define how user will observe
3D scene and its objects. Our definition says, that we want &#8736;30 degrees field of view with
width to height ration based on actual canvas size, `z=1` and `z=-1` distances to the near and far planes 
respectively.

This matrix used in our position equation as `uPMatrix` defined in vertex shared (explained below):
```
gl_Position = uPMatrix * uMVMatrix * vec4(aVertexPosition, 1.0);
```

`uPMatrix` in our case is constant and does not change its values with a time. In contrary to the
`uMVMatrix` which defines all current transformations applied to the world coordinates i.e. rotation, translation 
and etc.

All matrix operation implementations provided by `sylvester-es6` library  which is port of older `sylvester` 
library onto the ES6 rails.


Ok. We have only few lines left. And they are crucial for our rendering pipeline. I'm talking about
**lines 10-15**.

Here is the place where we actually have a call to `show3DScreen` method, which initializes GL context.

But before calling this method, we have guard construct waiting for all videos to be initialized and ready to
be played. We need this pre-condition as we will use videos image data as a textures for each side of our cube.
If we will try to do that before video is in ready state, we will get WebGL error which is unlikely thing we want 
to have.

So, we just periodically (every **50ms**) check all video components for their readiness status. When the guard condition
satisfied, we stop check interval and start our GL context init procedure.

Now, let's have a look on the most interesting piece of code of this component - `show3DScreen` method.

It consists of three logical parts:
- GL context preparation and init
    - Obtaining 3D context (`initWebGL`)
    - Vertices, textures and normals buffers creation (`initBuffers`)
    - Shader program compilation (`initShaders`)
    - Textures initialisation (`initTextures`)
- Rendering loop (`renderingLoop()`)
- Video playback start

The only thing that `initWebGL` method does, is GL context obtaining from the given `Canvas`:

```js
initWebGL(canvas) {
    try {
        return canvas.getContext("webgl") || canvas.getContext("experimental-webgl");
    } catch(e) {
        console.error("WebGL init failed", e);
        alert ("Unable to initialize WebGL. Your browser may not support it.");
        throw e;
    }
}
```

As some browser versions does not provide user with non-experimental `webgl` context, here we are trying to 
get at least one of them. In a case of failure, we will show user error message and propagate exception into the 
outer scope.

> **Note**: Sometimes, in order to get WebGL working (at least in Chrome), you need to disable hardware canvas acceleration 
as it prevents WebGL from functioning correctly on some devices. Here is instructions on how to fix this and few other 
possible issues: http://superuser.com/questions/836832/how-can-i-enable-webgl-in-my-browser

Assuming, that at this point you have GL context and it is safe and sound, we can continue to buffers and textures initialisation.

We use WebGL buffers in order to implement batch rendering operations for our scene objects providing WebGL with arrays of 
vertices, normals and texture coordinates. 

In method `initBuffers` we initialize 4 different buffer with a static data provided as an array of points. Here are these arrays:
- `cubeVerticesBuffer`: 
- `cubeVerticesNormalBuffer`:
- `cubeVerticesTextureCoordBuffer`:
- `cubeVerticesIndexBuffer`:

Textures initialisation method implement is trivial - it just creates 6 textures. We will use them in our rendering loop
to bind them to the actual texture data.

Now, as we have went though the basics of WebGL initialisation process, we can start overview of the application 
rendering loop.

Rendering loop function approximately 60 times per second. Particular number depends on browser and its refresh rate.

> **Note**
 To implement repeating of rendering loop function, we use `window.requestAnimationFrame`. This function delegates decision
about particular refresh rate and to the browser. Browser can also reduce refresh rate when page is not in active tag or
is in hidden `iframe`, which helps to optimise overall system performance. This is a preferred and '*best practices*' way of implementing animation on web pages. 

We can split it into the next stages:
1. Clearing world state
1. Binding textures 
1. Applying transformations
1. Binding drawing buffers
1. Drawing
 
##### 1. Clearing world state

This stage tells GL that it is a new drawing iteration and it should clear everything which was drawn before. To achieve this,
we need to clear all color and depth buffers.

These lines are responsible for this logic:
```js
50| gl.clearColor(0.0, 0.0, 0.0, 1.0);  
51| gl.clearDepth(1.0);                 
```

Just next to these lines you can find two more invocation to the WebGL API methods:
```js
gl.enable(gl.DEPTH_TEST);
gl.depthFunc(gl.LEQUAL);
```

By calling these functions, we tell WebGL to enable depth test which generally helps with positioning several objects
with respect to ordering between them. Even we have just a single object, I prefer to put keep these lines in rendering 
loop.

#### 2. Binding textures

On these stage we takes all our video components and bind them to each texture we created during the scene 
initialisation.

> Note: in many examples (and MDN article too) suggest using **MipMap** optimisation method for texture images. It is unsuitable in our case, as 
we our video does not pass (unless prepared) MIPS main requirement that texture image should be of size that is **power-of-2** 
(1, 2, 4, 8, 16, 32, 64, 128, 256, 512, 1024, or 2048 pixels).

Luckily, we don't need to do any extra operations in order to convert data frames from our `<video/>` component to 
WebGL format. This has already been sorted for us, as WebGL recognize `<video/>` component and know how to use it as a 
texture data.

Also, we are passing few additional parameters here (lines 58-61):
```js
gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);
gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE)
```

Here `TEXTURE_WRAP_S` and `TEXTURE_WRAP_T` prevents coordinates `s` and `t` respectively from repeating. We need this as due to
the fact that our texture is not power-of-2 and we must not allow its repeating.
 
 
#### Applying transformations

In our case we have just two transformation that we need to apply:
1. Moving camera to the right position, so we can see our cube properly
2. Applying transformation to the Model-View matrix, that affects how our cube will be displayed on the scene

Second step is interesting. What type of transformations we can apply to our cube? In our case we have just a 
single requirement of such sort - user can rotate cube with a mouse or touch. We will highlight topic 
of supporting mouse- and touch- based interactions later. Now we are only interested in rotation that is the result of
these interactions.

Ok. Rotation. In 3D scene we have 3 axis, obviously. We are going to provide user with ability to rotate object withing
two of them - OX and OY.

Here are the lines that responsible for this logic:
```js
if ( this.state.rotateY ) this.mvRotate(this.state.rotateY, [1, 0, 0]);
if ( this.state.rotateX ) this.mvRotate(this.state.rotateX, [0, 1, 0]);
```

It is as simple, as it looks like. If we have correct values in our component state for rotation within one of 
axis mentioned above, we are proceeding with rotation.

We're doing rotation here using matrix operations. We need to have angle (in radians) and a vector which represents axis where 
the rotation takes place.

> You can read more detailed overview about matrix operations and 3D-rotation in particular by following this link:
http://www.gamedev.net/page/resources/_/technical/math-and-physics/do-we-really-need-quaternions-r1199

When rotation matrix is ready, we just need to get a result of is multiplication by our Model-View matrix. 


#### Binding drawing buffers

Prior to actual drawing we need to bind drawing buffers to our GL context. WebGL will use data from these when 
`gl.drawElements()` method called.

#### Drawing

Last stage of our rendering loop. As we need to draw each side with its own texture, we need to split work onto 
6 steps - one for each side.

First of all, we need to activate texture by calling `gl.activateTexture(...)`. This will tell WebGL that it need to 
use texture (bound somewhere in a current stack) when drawing elements.

Next, we need to wire actual texture into the current scope by calling `gl.bindTexture(...)`. Texture should already be
configured and contains image data.

And now we can actually draw some graphics into the scene:
```js
1| gl.uniform1i(gl.getUniformLocation(shaderProgram, "uSampler"), 0);
2| gl.drawElements(gl.TRIANGLES, 6, gl.UNSIGNED_SHORT, 0);
```

At first line we tell our 2D-sampler (defined in our fragment shader) to use texture at address `0`. Then we push 6 vertices into the 
scene, which will form 2 triangles defining one of our cube faces.

And we need to do exactly the same for all 6 faces.
 
Done.

### Mouse/Touch interactions with a scene

So, the only thing left to be explained is interaction with a scene, as we need to rotate cube somehow.

Actually, it is the easiest part of all application we've build here. 

The general idea here is to convert differences between mouse movement on two axises into the angle, that
object need to be rotated for.

So, let's start with events. We need to track when user started holding mouse button/touch down on scene, when touch/mouse moves
across the scene and when he releases touch/mouse button.

We need to track down/up states of control in order to implement smooth rotation and make it more controllable from the 
user side. Means, that we will count only interactions made while in 'down' state, and ignore all of them while in 'up' 
state.

Here is the code:
```js
onRotationStarted(evt) {
    this.setState(update(this.state, {
        mouseDown: { $set: true },
        prevX: { $set: evt.clientX },
        prevY: { $set: evt.clientY }
    }));
}

onRotationFinished(evt) {
    this.setState(update(this.state, {
        mouseDown: { $set: false }
    }))
}
```

Easy. Just changing the state with proper values. Note, that we need to assign `prevX` and `prevY` in the 
`onRotationFinished` method, because otherwise coordinates from previous iteration will be used.

And here what the handler have for `onRotationMove` events:
```js
onRotationMove(evt) {
    if ( !this.state.mouseDown ) return;

    const x = evt.clientX;
    const y = evt.clientY;
    const prevX = this.state.prevX;
    const prevY = this.state.prevY;

    this.setState(update(this.state, {
        prevX: { $set: x },
        prevY: { $set: y },
        rotateX: { $set: this.state.rotateX + (x - prevX) / 5 },
        rotateY: { $set: this.state.rotateY + (y - prevY) / 5 }
    }))
}
```

#### ViewerStatic

Unfortunately, I didn't manage to implement this one due to the lack of time, but at least I can share some thoughts 
regarding this topic in text.

There are not many ways to show video without video support on the target device.

I can think about three possible ways:

**First way**
- Do not show video at all, and instead of video display - show him some notification with a link to Google Play/AppStore.
There he can download native application which is almost duplicate of the web site version, built with Cordova.

In such case we can easily replace video component with a native video play that will solve initial problem.

But this is a hard way. Not many people will install application and more than that - application on public market 
places require additional efforts to support and promote them.

**Second way**

Second way is to convert video to animated image (or set of animated images) and extract audio lines. Then synchronise them
on the client side, so audio starts only when GIF file is loaded and ready. WebP is even better.

Most obvious issue of this approach is the fact that the resulting image file will have a size much bigger than initial video 
file, if keeping the same quality options. But with proper tuning the resulting quality, frame rate and etc. we can get
feasible result.

There are quite few libraries available for dealing with video and animated images formats:
- JCodec - http://jcodec.org/ - read/write the most of popular media formats i.e. MP4, WMV, H.264, etc. 
- WebP ImageIO (https://github.com/lonnyj/webp-imageio)
- Gif4J (http://www.gif4j.com/) - create/modify GIF files
- Or even good old FFMPEG - maybe it is even much easier to just create scheduling converter which will use 
controlled FFmpeg process 



