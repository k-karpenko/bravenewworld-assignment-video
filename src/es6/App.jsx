import React from "react";

import { render }                                    from "react-dom"
import { hashHistory, Router, Route }                from "react-router"

import Viewport from "./components/Viewport.jsx"

import "./App.css";
function initialize() {
    window.requestAnimationFrame =  window.requestAnimationFrame ||
        window.mozRequestAnimationFrame ||
        window.webkitRequestAnimationFrame ||
        window.msRequestAnimationFrame;

    render(
        <Router history={hashHistory}>
            <Route path="/" component={Viewport}/>
        </Router>,
        document.getElementById("app")
    );
}

initialize()