import React, {Component} from "react"
import update from "react-addons-update";

import "./Viewer2D.css"
class Viewer2D extends Component {
    constructor(props) {
        super(props);

        this.state = {
            started: false,
            touchAvailable: false,
            maxTilt: 20,
            reverseTilt: false
        };

        this._refs = {};

        this.disableTilt = false;
        this.latestTilt = 0;
        this.centerOffset = 0;
        this.tiltCenterOffset = 0;
        this.tiltBarIndicatorWidth = 0;
        this.tiltBarWidth = 0;
        this.tiltBarPadding = 20;
        this.disableTilt = false;
        this.averageGamma = [];
        this.viewport = {};

        this.handleOrientation = this.handleOrientation.bind(this);
        this.updatePosition = this.updatePosition.bind(this);
    }

    componentDidMount() {
        window.addEventListener("deviceorientation", this.handleOrientation, true);
        // this.bindVideo();
    }

    componentDidUpdate() {
        const containerStyle = window.getComputedStyle(this._refs.container, null);

        this.viewport = {
            width: parseInt(containerStyle.width, 10),
            height: parseInt(containerStyle.height, 10)
        };

        this._refs.video.height = this.viewport.height;
        const resizedVideoWidth = (this.viewport.height * (this._refs.video.videoWidth / this._refs.video.videoHeight));
        this._refs.video.width = resizedVideoWidth;

        const delta = resizedVideoWidth - this.viewport.width;
        this.centerOffset = delta / 2;

        this.tiltBarWidth = this.viewport.width - this.tiltBarPadding;

        this.tiltBarIndicatorWidth = (this.viewport.width * this.tiltBarWidth) / resizedVideoWidth;
        this._refs.tiltBarIndicator.style.width = this.tiltBarIndicatorWidth + 'px';

        this.tiltCenterOffset = ((this.tiltBarWidth / 2) - (this.tiltBarIndicatorWidth / 2));

        this.updatePosition();

        if (this.tiltCenterOffset > 0) {
            this.disableTilt = false;
         } else {
            this.disableTilt = true; this.latestTilt = 0
         }
    }

    componentWillUnmount() {
        window.removeEventListener("deviceorientation", this.handleOrientation, true);
    }

    handleOrientation(event) {
        const absolute = event.absolute;
        const alpha    = event.alpha;
        const beta     = event.beta;
        const gamma    = event.gamma;

        if (!this.disableTilt) {
            if (this.averageGamma.length > 8) {
                this.averageGamma.shift();
            }

            this.averageGamma.push(event.gamma);

            this.latestTilt = this.averageGamma.reduce(function(a, b) { return a+b; }) / this.averageGamma.length;
        }

        window.requestAnimationFrame(() => this.updatePosition());
    }

    updatePosition() {
        let tilt = this.latestTilt;
        if (tilt > 0) {
            tilt = Math.min(tilt, this.state.maxTilt);
        } else {
            tilt = Math.max(tilt, this.state.maxTilt * -1);
        }

        if (!this.state.reverseTilt) {
            tilt = tilt * -1;
        }

        const pxToMove = (tilt * this.centerOffset) / this.maxTilt;

        this.updateVideoPosition(this._refs.video,  (this.centerOffset + pxToMove) * -1);
        this.updateTiltBar(tilt);

        window.requestAnimationFrame(() => this.updatePosition());
    }

    updateTiltBar(tilt) {
        const pxToMove = (tilt * ((this.tiltBarWidth - this.tiltBarIndicatorWidth) / 2)) / this.state.maxTilt;
        this.setTranslateX(this._refs.tiltBarIndicator, (this.tiltCenterOffset + pxToMove) );
    }

    updateVideoPosition(video, pxToMove) {
        this.setTranslateX(video, pxToMove);
    }

    setTranslateX(node, amount) {
        node.style.webkitTransform =
        node.style.MozTransform =
        node.style.msTransform =
        node.style.transform = "translateX(" + Math.round(amount) + "px)";
    }

    onUserInteraction(method) {
        if (method === 'm' && this.state.touchAvailable) return;

        if (!this.state.started) {
            this.setState(update(this.state, {
                touchAvailable: {$set: method === 't'},
                started: {$set: true}
            }), () => {
                this._refs.video.play()
            })
        } else {
            this.props.onMode3DEnabled()
        }
    }

    render() {
        return <div className="Viewer2D"
                    ref={c => this._refs.container = c}
                    onTouchEnd={() => this.onUserInteraction('t', new Date().getTime())}
                    onClick={() => this.onUserInteraction('m', new Date().getTime()) }
            >

            <div className="Viewer2D_tiltBar">
                <div className="Viewer2D_tiltIndicator"
                    ref={c => this._refs.tiltBarIndicator = c}
                 />
            </div>

            <video className={"Viewer2D__video"}
                poster={this.props.poster}
                ref={c => this._refs.video = c}>
                {this.props.sources.map(source =>
                    <source key={source.type} src={source.src} type={source.type}/>
                )}
            </video>
        </div>
    }

}

export default Viewer2D;