import React, {Component} from "react";

import {Matrix, Vector, makePerspective}             from "sylvester-es6"
import update from "react-addons-update";

import "./Viewer3D.css";

Math.radians = function(degrees) {
    return degrees * Math.PI / 180;
};

// Converts from radians to degrees.
Math.degrees = function(radians) {
    return radians * 180 / Math.PI;
};

class Viewer3D extends Component {

    constructor(props) {
        super(props);

        this._refs = {
            canvas3d: null,
                videos: []
        };

        this.mvMatrix = Matrix.I(4);
        this.mvMatrixStack = [];

        this.state = {
            tapTimes: 0,
            prevTapTime: -1,
            rotateX: 45,
            rotateY: 0,
            prevX: 0,
            prevY: 0
        };
    }

    initTextures(gl) {
        const textures = [];
        for ( let i = 0; i < 6; i ++ ) {
            textures[i] = gl.createTexture();
        }

        return textures;
    }

    renderingLoop(gl, videos, textures, shaderProgram) {
        if (videos.filter(v => v !== null && (v.paused || v.ended)).length > 0) return false;

        gl.clearColor(0.0, 0.0, 0.0, 1.0);
        gl.clearDepth(1.0);
        gl.enable(gl.DEPTH_TEST);
        gl.depthFunc(gl.LEQUAL);

        textures.forEach( (t, i) => {
            gl.bindTexture(gl.TEXTURE_2D, t);
            gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, true);
            gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, videos[i]);
            gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR);
            gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);
            gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
            gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
            gl.bindTexture(gl.TEXTURE_2D, null);
        } );


        this.mvMatrix = Matrix.I(4);
        this.mvMatrix = this.mvMatrix.x(Matrix.Translation(new Vector([0.0, 0.0, -5.0])).ensure4x4());

        if ( this.state.rotateY ) this.mvRotate(this.state.rotateY, [1, 0, 0]);
        if  ( this.state.rotateX ) this.mvRotate(this.state.rotateX, [0, 1, 0]);

        this.mvPushMatrix();

        gl.bindBuffer(gl.ARRAY_BUFFER, this.cubeVerticesBuffer);
        gl.vertexAttribPointer(this.vertexPositionAttribute, 3, gl.FLOAT, false, 0, 0);

        gl.bindBuffer(gl.ARRAY_BUFFER, this.cubeVerticesTextureCoordBuffer);
        gl.vertexAttribPointer(this.textureCoordAttribute, 2, gl.FLOAT, false, 0, 0);

        gl.bindBuffer(gl.ARRAY_BUFFER, this.cubeVerticesNormalBuffer);
        gl.vertexAttribPointer(this.vertexNormalAttribute, 3, gl.FLOAT, false, 0, 0);

        gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, this.cubeVerticesIndexBuffer);

        this.setMatrixUniforms(gl, shaderProgram);

        // side 1
        gl.activeTexture(gl.TEXTURE0);
        gl.bindTexture(gl.TEXTURE_2D, textures[0]);
        gl.uniform1i(gl.getUniformLocation(shaderProgram, "uSampler"), 0);
        gl.drawElements(gl.TRIANGLES, 6, gl.UNSIGNED_SHORT, 0);

        // side 2
        gl.activeTexture(gl.TEXTURE0)
        gl.bindTexture(gl.TEXTURE_2D, textures[1]);
        gl.uniform1i(gl.getUniformLocation(shaderProgram, "uSampler"), 0);
        gl.drawElements(gl.TRIANGLES, 6, gl.UNSIGNED_SHORT, 12);

        // side 3
        gl.activeTexture(gl.TEXTURE0)
        gl.bindTexture(gl.TEXTURE_2D, textures[2]);
        gl.uniform1i(gl.getUniformLocation(shaderProgram, "uSampler"), 0);
        gl.drawElements(gl.TRIANGLES, 6, gl.UNSIGNED_SHORT, 24);

        // side 4
        gl.activeTexture(gl.TEXTURE0)
        gl.bindTexture(gl.TEXTURE_2D, textures[3]);
        gl.uniform1i(gl.getUniformLocation(shaderProgram, "uSampler"), 0);
        gl.drawElements(gl.TRIANGLES, 6, gl.UNSIGNED_SHORT, 36);

        // side 5
        gl.activeTexture(gl.TEXTURE0)
        gl.bindTexture(gl.TEXTURE_2D, textures[4]);
        gl.uniform1i(gl.getUniformLocation(shaderProgram, "uSampler"), 0);
        gl.drawElements(gl.TRIANGLES, 6, gl.UNSIGNED_SHORT, 48);

        // side 6
        gl.activeTexture(gl.TEXTURE0)
        gl.bindTexture(gl.TEXTURE_2D, textures[5]);
        gl.uniform1i(gl.getUniformLocation(shaderProgram, "uSampler"), 0);
        gl.drawElements(gl.TRIANGLES, 6, gl.UNSIGNED_SHORT, 60);

        this.mvPopMatrix();

        window.requestAnimationFrame(() => this.renderingLoop(gl, videos, textures, shaderProgram));
    }

    show3DScreen(videos, canvas) {
        const gl = this.initWebGL(canvas);
        if ( gl ) {
            const shaderProgram = this.initShaders(gl);
            this.initBuffers(gl);
            const textures = this.initTextures(gl);

            videos[0].addEventListener('canplaythrough', () => this.renderingLoop(gl, videos, textures, shaderProgram), false);

            videos.filter(v => v !== null).forEach( (video, videoIndex) => {
                video.muted = true;
                video.currentTime = (videos[0].duration / videos.length) * videoIndex;
                video.play();
            });
        }
    }

    initShaders(gl) {
        const fragmentShader = this.getShader(gl, "shader-fs");
        const vertexShader = this.getShader(gl, "shader-vs");

        // Create the shader program

        const shaderProgram = gl.createProgram();
        gl.attachShader(shaderProgram, vertexShader);
        gl.attachShader(shaderProgram, fragmentShader);
        gl.linkProgram(shaderProgram);

        // If creating the shader program failed, alert

        if (!gl.getProgramParameter(shaderProgram, gl.LINK_STATUS)) {
            alert("Unable to initialize the shader program: " + gl.getProgramInfoLog(shader));
        }

        gl.useProgram(shaderProgram);

        this.vertexPositionAttribute = gl.getAttribLocation(shaderProgram, "aVertexPosition");
        gl.enableVertexAttribArray(this.vertexPositionAttribute);

        this.textureCoordAttribute = gl.getAttribLocation(shaderProgram, "aTextureCoord");
        gl.enableVertexAttribArray(this.textureCoordAttribute);

        this.vertexNormalAttribute = gl.getAttribLocation(shaderProgram, "aVertexNormal");
        gl.enableVertexAttribArray(this.vertexNormalAttribute);

        return shaderProgram;
    }

    getShader(gl, id) {
        const shaderScript = document.getElementById(id);

        // Didn't find an element with the specified ID; abort.

        if (!shaderScript) {
            return null;
        }

        // Walk through the source element's children, building the
        // shader source string.

        let theSource = "";
        let currentChild = shaderScript.firstChild;

        while(currentChild) {
            if (currentChild.nodeType == 3) {
                theSource += currentChild.textContent;
            }

            currentChild = currentChild.nextSibling;
        }

        // Now figure out what type of shader script we have,
        // based on its MIME type.

        var shader;

        if (shaderScript.type == "x-shader/x-fragment") {
            shader = gl.createShader(gl.FRAGMENT_SHADER);
        } else if (shaderScript.type == "x-shader/x-vertex") {
            shader = gl.createShader(gl.VERTEX_SHADER);
        } else {
            return null;  // Unknown shader type
        }

        // Send the source to the shader object

        gl.shaderSource(shader, theSource);

        // Compile the shader program

        gl.compileShader(shader);

        // See if it compiled successfully

        if (!gl.getShaderParameter(shader, gl.COMPILE_STATUS)) {
            alert("An error occurred compiling the shaders: " + gl.getShaderInfoLog(shader));
            return null;
        }

        return shader;
    }

    initBuffers(gl) {
        this.cubeVerticesBuffer = gl.createBuffer();
        gl.bindBuffer(gl.ARRAY_BUFFER, this.cubeVerticesBuffer);

        const vertices = [
            // Front face
            -1.0, -1.0,  1.0,
            1.0, -1.0,  1.0,
            1.0,  1.0,  1.0,
            -1.0,  1.0,  1.0,

            // Back face
            -1.0, -1.0, -1.0,
            -1.0,  1.0, -1.0,
            1.0,  1.0, -1.0,
            1.0, -1.0, -1.0,

            // Top face
            -1.0,  1.0, -1.0,
            -1.0,  1.0,  1.0,
            1.0,  1.0,  1.0,
            1.0,  1.0, -1.0,

            // Bottom face
            -1.0, -1.0, -1.0,
            1.0, -1.0, -1.0,
            1.0, -1.0,  1.0,
            -1.0, -1.0,  1.0,

            // Right face
            1.0, -1.0, -1.0,
            1.0,  1.0, -1.0,
            1.0,  1.0,  1.0,
            1.0, -1.0,  1.0,

            // Left face
            -1.0, -1.0, -1.0,
            -1.0, -1.0,  1.0,
            -1.0,  1.0,  1.0,
            -1.0,  1.0, -1.0
        ];
        gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(vertices), gl.STATIC_DRAW);

        this.cubeVerticesNormalBuffer = gl.createBuffer();
        gl.bindBuffer(gl.ARRAY_BUFFER, this.cubeVerticesNormalBuffer);

        const vertexNormals = [
            // Front
            0.0,  0.0,  1.0,
            0.0,  0.0,  1.0,
            0.0,  0.0,  1.0,
            0.0,  0.0,  1.0,

            // Back
            0.0,  0.0, -1.0,
            0.0,  0.0, -1.0,
            0.0,  0.0, -1.0,
            0.0,  0.0, -1.0,

            // Top
            0.0,  1.0,  0.0,
            0.0,  1.0,  0.0,
            0.0,  1.0,  0.0,
            0.0,  1.0,  0.0,

            // Bottom
            0.0, -1.0,  0.0,
            0.0, -1.0,  0.0,
            0.0, -1.0,  0.0,
            0.0, -1.0,  0.0,

            // Right
            1.0,  0.0,  0.0,
            1.0,  0.0,  0.0,
            1.0,  0.0,  0.0,
            1.0,  0.0,  0.0,

            // Left
            -1.0,  0.0,  0.0,
            -1.0,  0.0,  0.0,
            -1.0,  0.0,  0.0,
            -1.0,  0.0,  0.0
        ];

        gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(vertexNormals), gl.STATIC_DRAW);

        this.cubeVerticesTextureCoordBuffer = gl.createBuffer();
        gl.bindBuffer(gl.ARRAY_BUFFER, this.cubeVerticesTextureCoordBuffer);

        const textureCoordinates = [
            // Front
            0.0,  0.0,
            1.0,  0.0,
            1.0,  1.0,
            0.0,  1.0,
            // Back
            0.0,  0.0,
            1.0,  0.0,
            1.0,  1.0,
            0.0,  1.0,
            // Top
            0.0,  0.0,
            1.0,  0.0,
            1.0,  1.0,
            0.0,  1.0,
            // Bottom
            0.0,  0.0,
            1.0,  0.0,
            1.0,  1.0,
            0.0,  1.0,
            // Right
            0.0,  0.0,
            1.0,  0.0,
            1.0,  1.0,
            0.0,  1.0,
            // Left
            0.0,  0.0,
            1.0,  0.0,
            1.0,  1.0,
            0.0,  1.0
        ];

        gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(textureCoordinates), gl.STATIC_DRAW);

        this.cubeVerticesIndexBuffer = gl.createBuffer();
        gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, this.cubeVerticesIndexBuffer);

        const cubeVertexIndices = [
            0,  1,  2,      0,  2,  3,    // front
            4,  5,  6,      4,  6,  7,    // back
            8,  9,  10,     8,  10, 11,   // top
            12, 13, 14,     12, 14, 15,   // bottom
            16, 17, 18,     16, 18, 19,   // right
            20, 21, 22,     20, 22, 23    // left
        ];

        gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, new Uint16Array(cubeVertexIndices), gl.STATIC_DRAW);
    }

    initWebGL(canvas) {
        try {
            return canvas.getContext("webgl") || canvas.getContext("experimental-webgl");
        } catch(e) {
            console.error("WebGL init failed", e);
            alert ("Unable to initialize WebGL. Your browser may not support it.");
            throw e;
        }
    }

    componentWillMount() {
        window.addEventListener("keypress", this.onKeyPress);
    }

    componentWillUnmount() {
        window.removeEventListener("keypress", this.onKeyPress);
    }

    componentDidMount() {
        const videos = this._refs.videos;
        const canvas3d = this._refs.canvas3d;

        canvas3d.width = canvas3d.clientWidth;
        canvas3d.height = canvas3d.clientHeight;

        this.perspectiveMatrix = makePerspective(30, canvas3d.width / canvas3d.height, 1, -5);

        const metaFetchInterval = setInterval(() => {
            if (videos.filter(v => v !== null && v.readyState <= 0).length === 0) {
                this.show3DScreen(videos, canvas3d);
                clearInterval(metaFetchInterval);
            }
        }, 50);
    }

    onRotationMove(x, y) {
        if ( !this.state.mouseDown ) return;

        const prevX = this.state.prevX;
        const prevY = this.state.prevY;

        this.setState(update(this.state, {
            prevX: { $set: x },
            prevY: { $set: y },
            rotateX: { $set: this.state.rotateX + (x - prevX) / 5 },
            rotateY: { $set: this.state.rotateY + (y - prevY) / 5 }
        }))
    }

    onRotationStarted(clientX, clientY) {
        this.setState(update(this.state, {
            prevTapTime: { $set: new Date().getTime() },
            mouseDown: { $set: true },
            prevX: { $set: clientX },
            prevY: { $set: clientY }
        }));
    }

    onRotationFinished() {
        this.setState(update(this.state, {
            mouseDown: { $set: false }
        }))
    }

    render() {
        const {sources} = this.props;

        const sides = [];
        for ( let i = 0 ; i < 6; i ++ ) {
            sides.push("video-" + i);
        }

        return <div className="Viewer3D__scene">
            <canvas onMouseDown={e => this.onRotationStarted(e.clientX, e.clientY)}
                    onTouchStart={e => this.onRotationStarted(e.touches[0].pageX, e.touches[0].pageY)}
                    onMouseUp={e => this.onRotationFinished()}
                    onTouchEnd={e => this.onRotationFinished() }
                    onTouchMove={e => this.onRotationMove(e.touches[0].pageX, e.touches[0].pageY)}
                    onMouseMove={e => this.onRotationMove(e.clientX, e.clientY)}
                    className="Viewer3D__canvas"
                    ref={c => this._refs.canvas3d = c}/>
            {sides.map(side =>
                <video loop key={side.toString()} className="Viewer3D__video hidden"
                       ref={c => this._refs.videos.push(c)}>
                    {sources.map(source =>
                        <source key={source.src} src={source.src} type={source.type}/>
                    )}
                </video>
            )}
        </div>
    }

    ////
    // -- 3D helper methods
    ////

    setMatrixUniforms(gl, shaderProgram) {
        const pUniform = gl.getUniformLocation(shaderProgram, "uPMatrix");
        gl.uniformMatrix4fv(pUniform, false, new Float32Array(this.perspectiveMatrix.flatten()));

        const mvUniform = gl.getUniformLocation(shaderProgram, "uMVMatrix");
        gl.uniformMatrix4fv(mvUniform, false, new Float32Array(this.mvMatrix.flatten()));

        let normalMatrix = this.mvMatrix.inverse();
        normalMatrix = normalMatrix.transpose();
        const nUniform = gl.getUniformLocation(shaderProgram, "uNormalMatrix");
        gl.uniformMatrix4fv(nUniform, false, new Float32Array(normalMatrix.flatten()));
    }

    mvPushMatrix(m) {
        if (m) {
            this.mvMatrixStack.push(m.dup());
            this.mvMatrix = m.dup();
        } else {
            this.mvMatrixStack.push(this.mvMatrix.dup());
        }
    }

    mvPopMatrix() {
        if (!this.mvMatrixStack.length) {
            throw("Can't pop from an empty matrix stack.");
        }

        this.mvMatrix = this.mvMatrixStack.pop();
        return this.mvMatrix;
    }

    mvRotate(angle, v) {
        this.mvMatrix = this.mvMatrix.x(
            Matrix.Rotation(Math.radians(angle), new Vector([v[0], v[1], v[2]])).ensure4x4()
        );
    }

}

export default Viewer3D;