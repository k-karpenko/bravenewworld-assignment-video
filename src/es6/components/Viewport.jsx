import React, {Component} from "react"
import update from "react-addons-update";
import Viewer2D from "./Viewer2D.jsx";
import Viewer3D from "./Viewer3D.jsx";

class Viewport extends Component {

    constructor(props) {
        super(props);

        this.state = {
            poster: "/img/460229183.webp",
            source: [{
                src : "/video/217624774.webm",
                type: "video/webm"
            }, {
                src: "/video/217624774.mp4",
                type: "video/mp4"
            }, {
                src: "/video/217624774.wmv",
                type: "video/wmv"
            }],
            mode: '2d'
        };
    }

    onMode2DEnabled() {
        this.setState(update(this.state, { mode: { $set: '2d' } }));
    }

    onMode3DEnabled() {
        this.setState(update(this.state, { mode: { $set: '3d' } }));
    }

    render() {
        switch(this.state.mode) {
            case '2d':
                return <Viewer2D
                    poster={this.state.poster}
                    sources={this.state.source}
                    onMode3DEnabled={e => this.onMode3DEnabled()}
                />;
            case '3d':
                return <Viewer3D
                    sources={this.state.source}
                    onMode2DEnabled={e => this.onMode2DEnabled()}
                />;
            default:
                return <div>Unknown mode value provided</div>
        }
    }

}

export default Viewport