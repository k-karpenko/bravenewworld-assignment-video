var path                    = require('path');
var webpack                 = require('webpack');
var CopyWebpackPlugin       = require('copy-webpack-plugin');
var autoprefixer            = require('autoprefixer');

module.exports = {

    entry: './src/es6/App.jsx',

    resolve: {
        extensions: ['', '.js', '.jsx']
    },

    output: {
        path: 'www',
        filename: "app.js",
        library: 'www'
    },

    module: {
        preLoaders: [
            {
                test: /\.tsx?$/,
                loader: "tslint"
            }
        ],
        loaders: [
            {
                test: /\.jsx?$/,
                loader: "babel-loader",
                exclude: /node_modules/
            },

            { test: /\.css$/, loader: "style-loader!css-loader!postcss-loader" }
        ]
    },

    plugins: [
        new CopyWebpackPlugin([
            { from: 'src/video', to: 'video' },
            { from: 'src/es5', to: 'es5' },
            { from: 'src/img', to: 'img' },
            { from: 'src/icons', to: 'icons' },
            { from: 'src/css', to: 'css' },
            { from: 'src/font', to: 'font' }
        ])
    ],

    postcss: function(webpack) {
        return [
            autoprefixer({ browsers: ['last 2 versions'] })
        ]
    },

    externals: {
        "promise": "Promise"
    },

    resolveLoader: {
        root: path.join(__dirname, 'node_modules')
    }
};
